autowatch = 1;

var url = "http://192.168.129.77:8080/get?";
var what_to_get = ["magX", "magY", "magZ", "mag"];

function phyphox_data() {
    var response = "";
    var req = new XMLHttpRequest();
    req.onreadystatechange = function() {
        if (req.readyState == 4 && req.status == 200) {
            response = JSON.parse(req.responseText);
            for (var i = 0; i < what_to_get.length; i++) {
                var item = what_to_get[i];
                var mag_data = response.buffer[item].buffer[0];
                outlet(i, item + ": " + mag_data);
            }
        }
    }
    req.open("GET", url + what_to_get.join("&"), true);
    req.send();
}

function bang() {
    phyphox_data();
}