# Android Sensor Data Retrieval using Phyphox and Python

This project aims to retrieve sensor data from an Android device using the Phyphox application and a Python script running on a PC. Phyphox is a mobile application that allows users to conduct physics experiments using various sensors available on smartphones.

## Requirements
- Android device with the Phyphox application installed
- PC with Python installed
- `requests` library for Python (can be installed via pip)

## Setup
1. Ensure that your Android device and PC are connected to the same local network.
2. Open the Phyphox application on your Android device.
3. Configure the desired sensors in Phyphox and start the experiment.
4. Run the provided Python script (`phyphox_data.py`) on your PC.

## Usage
1. Modify the `ur` variable in the Python script (`phyphox_data.py`) to match the IP address and port number of your Android device running Phyphox.
2. Adjust the `what_to_get` list in the script to specify which sensor data you want to retrieve. Available options include 'magX', 'magY', 'magZ', and 'mag' for magnetometer data, but you can extend this list according to the sensors you're using in Phyphox.
3. Run the Python script. It will continuously retrieve and print sensor data from the Phyphox experiment running on your Android device.
4. Press `Ctrl + C` to stop the script when you're done collecting data.

## Example
```python
import requests as r
import json
import time

ur = 'http://192.168.2.42:8080/get?'
what_to_get = ['magX', 'magY', 'magZ', 'mag']

def phyphox_data():
    response = r.get(url + '&'.join(what_to_get)).text
    data = json.loads(response)
    for item in what_to_get:
        mag_data = data['buffer'][item]['buffer'][0]
        print(f'{mag_data:10.7}', end='\t')
    print('')

while True:
    phyphox_data()
    time.sleep(0.1)

```

## Notes

- Ensure that your Android device's IP address remains consistent or update the IP address in the Python script accordingly if it changes.

- This script provides a basic example of retrieving sensor data from Phyphox. You can extend it to include additional functionality or processing according to your requirements.

## License

This project is licensed under the MIT License.
