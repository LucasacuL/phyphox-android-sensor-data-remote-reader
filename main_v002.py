import requests as r
import json
import time

# URL to fetch data from Phyphox app
url = 'http://192.168.129.77:8080/get?'

# Define the parameters you want to fetch from Phyphox
what_to_get = ['magX', 'magY', 'magZ', 'mag']

def phyphox_data():
    # Fetch data from Phyphox
    response = r.get(url + '&'.join(what_to_get)).text
    data = json.loads(response)
    
    # Iterate over each parameter and print its value
    for item in what_to_get:
        mag_data = data['buffer'][item]['buffer'][0]
        print(f'{item}: {mag_data}', end='\t')
    print('')

# Continuously fetch and print magnetometer data
while True:
    phyphox_data()
    time.sleep(0.1)
